use std::env;
use std::path::PathBuf;

fn main() -> Result<(), Box<dyn std::error::Error>> {
    let out_dir = PathBuf::from(env::var("OUT_DIR").unwrap());

    tonic_build::configure()
        .build_client(false)
        .file_descriptor_set_path(out_dir.join("authserver_proto_descriptor.bin"))
        .compile(
            &["proto/resource.proto", "proto/authorization.proto"],
            &["proto"],
        )?;

    Ok(())
}
