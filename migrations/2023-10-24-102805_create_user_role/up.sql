-- Your SQL goes here

CREATE TABLE "user" (
  id uuid PRIMARY KEY,
  username varchar(64) NOT NULL UNIQUE,
  email varchar(64) NOT NULL UNIQUE,
  firstname varchar(128) NOT NULL,
  lastname varchar(255) NOT NULL,
  picture text,
  locale varchar(8),
  password text
);

CREATE TYPE "permission_effect" AS ENUM ('allow', 'deny');

CREATE TABLE "principal" (
  user_id uuid PRIMARY KEY,
  allowed_domains TEXT[] NOT NULL DEFAULT '{}',
  all_domains_allowed boolean NOT NULL DEFAULT false
);

CREATE TABLE "permission" (
  permission_name TEXT NOT NULL,
  resource_name TEXT NOT NULL,
  actions TEXT[] NOT NULL DEFAULT '{}',
  effect permission_effect NOT NULL DEFAULT 'deny',

  CONSTRAINT PK_permission PRIMARY KEY (permission_name),
  CONSTRAINT UNIQUE_permission UNIQUE (permission_name, resource_name, actions, effect)
);

CREATE TABLE "user_permission" (
  principal_id uuid NOT NULL,
  permission_name TEXT  NOT NULL,
  domain TEXT NOT NULL,
  resource_name TEXT  NOT NULL,
  actions TEXT[] NOT NULL DEFAULT '{}',
  effect permission_effect NOT NULL DEFAULT 'deny',

  CONSTRAINT PK_user_permission PRIMARY KEY (principal_id, permission_name, domain),
  CONSTRAINT FK_user_permission_principal FOREIGN KEY (principal_id) REFERENCES principal (user_id)
);

CREATE TABLE "role" (
  name TEXT,
  domain TEXT NOT NULL DEFAULT '',
  description TEXT NOT NULL DEFAULT '',

  CONSTRAINT PK_role PRIMARY KEY (name, domain)
);

CREATE TABLE "user_role" (
  principal_id uuid NOT NULL,
  role_name TEXT NOT NULL,
  domain TEXT NOT NULL DEFAULT '',

  CONSTRAINT PK_user_role PRIMARY KEY (principal_id, role_name, domain),

  CONSTRAINT FK_user_role_principal FOREIGN KEY (principal_id) REFERENCES principal (user_id),
  CONSTRAINT FK_user_role_role FOREIGN KEY (role_name, domain) REFERENCES role (name, domain)
);

CREATE TABLE "role_permission" (
  role_name TEXT NOT NULL,
  permission_name TEXT NOT NULL,
  domain TEXT NOT NULL DEFAULT '',

  CONSTRAINT PK_role_permission PRIMARY KEY (role_name, permission_name, domain),

  CONSTRAINT FK_role_permission_role FOREIGN KEY (role_name, domain) REFERENCES role (name, domain),
  CONSTRAINT FK_role_permission_permission FOREIGN KEY (permission_name) REFERENCES permission (permission_name)
);

