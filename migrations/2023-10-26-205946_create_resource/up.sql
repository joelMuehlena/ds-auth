-- Your SQL goes here

CREATE TABLE resource_reference (
    resource_name TEXT NOT NULL,
    domain TEXT NOT NULL,
    reference_id UUID NOT NULL,
    actions TEXT[] NOT NULL,
    attributes jsonb NOT NULL,

    CONSTRAINT PK_resource_reference PRIMARY KEY (resource_name, domain, reference_id),
    CONSTRAINT UNIQUE_resource_reference UNIQUE (resource_name, reference_id)
);