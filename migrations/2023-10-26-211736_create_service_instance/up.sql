-- Your SQL goes here

CREATE TABLE service_instance (
    "name" TEXT NOT NULL,
    service_id UUID NOT NULL,
    health_check_url TEXT NOT NULL,

    created_at TIMESTAMPTZ NOT NULL DEFAULT (now() at time zone 'utc'),
    updated_at TIMESTAMPTZ NOT NULL DEFAULT (now() at time zone 'utc'),

    CONSTRAINT PK_service_instance PRIMARY KEY (name, service_id)
);

CREATE TABLE service_resource_claim (
    service_name TEXT NOT NULL,
    service_id UUID NOT NULL,

    reference_id UUID NOT NULL,
    resource_name TEXT NOT NULL,

    CONSTRAINT PK_service_resource_claims PRIMARY KEY (service_name, service_id, reference_id, resource_name),
    CONSTRAINT FK_service_resource_claims_service_instance FOREIGN KEY (service_name, service_id) REFERENCES service_instance (name, service_id),
    CONSTRAINT FK_service_resource_claims_resource_reference FOREIGN KEY (resource_name, reference_id) REFERENCES resource_reference (resource_name, reference_id) ON DELETE CASCADE
);