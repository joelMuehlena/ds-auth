use actix_web::{
    cookie::{time, Cookie},
    post, web, HttpRequest, HttpResponse, Result,
};
use futures_util::StreamExt as _;
use serde::{Deserialize, Serialize};

use crate::api::headers::get_content_type;
use crate::{auth_type, IAMLock, ProviderData};

#[derive(Debug, Deserialize)]
pub struct AuthorizeRequest {
    provider: Option<String>,
    with_refresh: Option<bool>,
}

#[derive(Debug, Serialize)]
pub struct AuthorizeResponse {
    code: u8,
    access_token: String,
    #[serde(skip_serialize_if = "Option::is_none")]
    refresh_token: Option<String>,
}

#[post("/authorize")]
pub async fn authorize(
    req: HttpRequest,
    mut body: web::Payload,
    iam: web::Data<IAMLock>,
    query: web::Query<AuthorizeRequest>,
) -> Result<HttpResponse, actix_web::Error> {
    let provider_name = &query
        .provider
        .clone()
        .ok_or(actix_web::error::ErrorBadRequest("No provider specified"))?;

    let mut bytes = web::BytesMut::new();
    while let Some(item) = body.next().await {
        let item = item?;
        bytes.extend_from_slice(&item);
    }

    let content_type =
        get_content_type(&req).ok_or(actix_web::error::ErrorBadRequest("No content type"))?;

    let provider_data = ProviderData {
        content_type: content_type.to_string(),
        data: bytes,
    };

    let iam = iam.read().map_err(|e| {
        actix_web::error::ErrorInternalServerError(format!("Failed to get IAM service: {:?}", e))
    })?;

    let authorize_result = match provider_name.to_lowercase().as_str() {
        "email" => {
            iam.authorize(auth_type::AuthType::Email, provider_data)
                .await
        }
        _ => return Err(actix_web::error::ErrorBadRequest("Provider does not exist")),
    }
    .map_err(|e| actix_web::error::ErrorUnauthorized(e))?;

    let (_, refresh_token_lifetime) = iam.token_lifetimes();

    let refresh_cookie_cfg = iam.config.api.cookie.clone();

    drop(iam);

    let cookie_lifetime: time::Duration =
        time::Duration::seconds(refresh_token_lifetime.num_seconds());

    let cookie = Cookie::build("refresh_token", authorize_result.1.clone())
        .domain(refresh_cookie_cfg.domain)
        .path(refresh_cookie_cfg.path)
        .secure(refresh_cookie_cfg.secure)
        .http_only(refresh_cookie_cfg.http_only)
        .max_age(cookie_lifetime)
        .finish();

    let refresh_token = query
        .with_refresh
        .unwrap_or(false)
        .then(|| authorize_result.1);

    Ok(HttpResponse::Ok().cookie(cookie).json(AuthorizeResponse {
        access_token: authorize_result.0,
        refresh_token,
        code: 200,
    }))
}
