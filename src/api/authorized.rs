use actix_web::{get, web, HttpRequest, HttpResponse};
use serde::{Deserialize, Serialize};

use crate::api::headers::get_auth_header;
use crate::error::IAMError;
use crate::{models, IAMLock};

#[derive(Debug, Deserialize)]
pub struct AuthorizedRequest {
    permission: Option<String>,
}

#[derive(Debug, Serialize)]
pub struct VerifyResponse {
    code: u16,
    error: Option<String>,
    attributes: models::ResourceReferenceAttributes,
}

#[get("/authorized")]
pub async fn authorized(
    req: HttpRequest,
    iam: web::Data<IAMLock>,
    query: web::Query<AuthorizedRequest>,
) -> Result<HttpResponse, actix_web::Error> {
    let auth = match get_auth_header(&req) {
        Some(auth) => auth,
        None => {
            return Ok(HttpResponse::Unauthorized().json(VerifyResponse {
                code: 401,
                error: Some("Missing Authentication header".to_string()),
                attributes: models::ResourceReferenceAttributes::default(),
            }));
        }
    };

    let requested_permission = query.permission.clone();

    if let None = requested_permission {
        return Ok(HttpResponse::BadRequest().json(VerifyResponse {
            code: 400,
            error: Some("Missing permission query parameter".to_string()),
            attributes: models::ResourceReferenceAttributes::default(),
        }));
    }

    let requested_permission = requested_permission
        .expect("Cannot unwrap requested_permission but is not none. Should be unreachable");

    let iam = iam.read().map_err(|e| {
        actix_web::error::ErrorInternalServerError(format!("Failed to get IAM service: {:?}", e))
    })?;

    let result: Result<(bool, models::ResourceReferenceAttributes), IAMError> =
        iam.is_authorized(auth, requested_permission.clone());

    match result {
        Ok((is_authorized, attributes)) => {
            if is_authorized {
                return Ok(HttpResponse::Ok().json(VerifyResponse {
                    code: 200,
                    error: None,
                    attributes,
                }));
            }
            return Ok(HttpResponse::Unauthorized().json(VerifyResponse {
                code: 401,
                error: Some(format!(
                    "Not authorized to perform {}: Insufficient permissions",
                    requested_permission
                )),
                attributes,
            }));
        }
        Err(e) => Ok(HttpResponse::Unauthorized().json(VerifyResponse {
            code: 401,
            error: Some(format!(
                "Not authorized to perform {}: {}",
                requested_permission,
                e.to_string()
            )),
            attributes: models::ResourceReferenceAttributes::default(),
        })),
    }
}
