use actix_web::HttpRequest;

pub fn get_content_type<'a>(req: &'a HttpRequest) -> Option<&'a str> {
    req.headers().get("content-type")?.to_str().ok()
}

pub fn get_auth_header<'a>(req: &'a HttpRequest) -> Option<String> {
    let val = match req.headers().get("authorization")?.to_str() {
        Ok(val) => val,
        Err(_) => return None,
    };

    if val.is_empty() {
        return None;
    }

    if val.starts_with("Bearer") {
        let val = val.replace("Bearer", "").trim_start().to_owned();
        return Some(val);
    }

    None
}

// pub fn get_req_instance_header<'a>(req: &'a HttpRequest) -> Option<&'a str> {
//     req.headers().get("X-Req-Instance")?.to_str().ok()
// }

// pub fn get_is_debug_enabled<'a>(req: &'a HttpRequest) -> bool {
//     match req
//         .headers()
//         .get("X-Enable-Server-Debug")
//         .map_or("false", |f| f.to_str().unwrap_or("false"))
//     {
//         "true" => true,
//         _ => false,
//     }
// }
