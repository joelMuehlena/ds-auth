use actix_web::{get, web, HttpResponse};
use serde::Serialize;

use crate::{iam::Meta, meta::TypedMetaInfo, IAMLock};

#[derive(Debug, Serialize)]
pub struct Response {
    code: u8,
    meta: Meta,
}

#[get("/meta")]
pub async fn get(iam: web::Data<IAMLock>) -> Result<HttpResponse, actix_web::Error> {
    let iam = iam.read().map_err(|e| {
        actix_web::error::ErrorInternalServerError(format!("Failed to get IAM service: {:?}", e))
    })?;

    let meta = iam.concrete_meta();

    Ok(HttpResponse::Ok().json(Response { code: 200, meta }))
}
