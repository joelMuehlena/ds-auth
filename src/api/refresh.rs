use actix_web::{
    cookie::{time::Duration, Cookie},
    get, web, HttpRequest, HttpResponse, Result,
};
use serde::Serialize;

use crate::IAMLock;

#[derive(Debug, Serialize)]
pub struct RefreshOkResponse {
    code: u16,
    access_token: String,
}

#[derive(Debug, Serialize)]
pub struct RefreshErrorResponse {
    code: u16,
    error: String,
}

#[get("/refresh")]
pub async fn refresh(
    req: HttpRequest,
    iam: web::Data<IAMLock>,
) -> Result<HttpResponse, actix_web::Error> {
    let refresh_token = match req.cookie("refresh_token") {
        Some(token) => token.value().to_owned(),
        None => {
            return Err(actix_web::error::ErrorUnauthorized(
                "Failed to get refresh token please login first",
            ))
        }
    };

    let iam = iam.read().map_err(|e| {
        actix_web::error::ErrorInternalServerError(format!("Failed to get IAM service: {:?}", e))
    })?;

    let authorize_result = iam
        .refresh(refresh_token)
        .await
        .map_err(|e| actix_web::error::ErrorUnauthorized(e));

    let mut cookie = Cookie::build("refresh_token", "")
        .domain(iam.config.api.cookie.domain.clone())
        .path(iam.config.api.cookie.path.clone())
        .secure(iam.config.api.cookie.secure)
        .http_only(iam.config.api.cookie.http_only)
        .max_age(Duration::minutes(iam.config.api.cookie.max_age_minutes))
        .finish();

    drop(iam);

    match authorize_result {
        Err(e) => {
            cookie.make_removal();

            Ok(HttpResponse::Unauthorized()
                .cookie(cookie)
                .json(RefreshErrorResponse {
                    code: 401,
                    error: e.to_string(),
                }))
        }
        Ok(authorize_result) => {
            cookie.set_value(authorize_result.1);
            Ok(HttpResponse::Ok().cookie(cookie).json(RefreshOkResponse {
                access_token: authorize_result.0,
                code: 200,
            }))
        }
    }
}
