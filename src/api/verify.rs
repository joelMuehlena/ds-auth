use actix_web::{get, web, HttpRequest, HttpResponse, Result};
use serde::Serialize;

use crate::api::headers::get_auth_header;
use crate::IAMLock;

#[derive(Debug, Serialize)]
pub struct VerifyResponse {
    code: u16,
}

#[get("/verify")]
pub async fn verify(
    req: HttpRequest,
    iam: web::Data<IAMLock>,
) -> Result<HttpResponse, actix_web::Error> {
    let auth = match get_auth_header(&req) {
        Some(auth) => auth,
        None => {
            return Ok(HttpResponse::Unauthorized().json(VerifyResponse { code: 401 }));
        }
    };

    let iam = iam.read().map_err(|e| {
        actix_web::error::ErrorInternalServerError(format!("Failed to get IAM service: {:?}", e))
    })?;

    iam.verify(auth.to_string())
        .map_err(|e| actix_web::error::ErrorUnauthorized(e))?;

    drop(iam);

    Ok(HttpResponse::Ok().json(VerifyResponse { code: 200 }))
}
