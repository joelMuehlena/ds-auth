use thiserror::Error;

#[derive(Debug, Error)]
pub enum AuthorizeError {
    #[error(
        "Failed to parse permission string. Expected format: domain::resource_name::action::effect"
    )]
    WrongSizedPermissionString,

    #[error("Action not possible on resource: {0}")]
    ActionNotPossibleOnResource(String),

    #[error("Failed to fetch principal from database")]
    FailedToFetchPrincipalFromDatabase(#[from] diesel::result::Error),

    #[error("Resource not existing in specified domain: {0}::{1}")]
    ResourceNotExistingInSpecifiedDomain(String, String),

    #[error("User not allowed to access domain: {0}")]
    UserNotAllowedTOperateOnDomain(String),

    #[error("Failed to fetch resource from database, because the pool returned an error")]
    DBPoolError(#[from] r2d2::Error),

    #[error("Any error occurred: {0:?}")]
    Any(Option<String>),
}
