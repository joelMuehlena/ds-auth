use std::{collections::HashSet, vec};

use crate::meta::{self, MetaInfo, TypedMetaInfo};
use crate::resource::Resource;
use crate::DBPool;

use crate::{authorization::error::AuthorizeError, models, schema};
use serde::{Deserialize, Serialize};
use tracing::debug;
use uuid::Uuid;

use diesel::prelude::*;

/*

Possibilities:

Example users: Alice, Bob
Example groups: Admins, Users, BlogAuthor, CMSCreator
Example domains: CMS, Blog, "",

Resources register themselves on startup and get a unique id.
  - "same" resources of e.g. scaled services get new id and are tracked as references.
      If a resource is deleted, all references are deleted as well.
      If all references are deleted, the resource is deleted as well.
  - Resources can be registered with a domain prefix, e.g. "CMS:EducationEntry"
  - Resources and principals can have arbitrary attributes

Scheme:
  <domain>:<role_type>:<role_resource>:<role_action>:<role_effect(allow/deny)>

  User -> Permission X
  User -> Permission Y
  User -> Permission Z

  User -> Role A -> Permission X
  User -> Role A -> Permission Y
  User -> Role B -> Permission Z

  User -> Group A -> Role A -> Permission X
  User -> Group A -> Role B -> Permission Z

  User -> Role C -> Group A -> Permission X

  - blog:u:Alice:Post:Create
  - cms:g:CMSCreator:EducationEntry:*
  - cms:u:Bob:EducationEntry:Create
  - cms:m:Users:Bob,Alice

*/

#[derive(Debug)]
pub struct Principal {
    user_id: Uuid,
    allowed_domains: Vec<Option<String>>,
    all_domains_allowed: bool,

    user_permissions: Vec<models::UserPermission>,
    user_roles: Vec<Role>,
}

#[derive(Debug)]

pub struct Role {
    name: String,
    domain: String,
    description: String,
    permissions: Vec<models::Permission>,
}

impl Principal {
    pub fn user_to_principal(pool: &DBPool, id: &Uuid) -> Result<Self, AuthorizeError> {
        Principal::fetch(pool, id)
    }

    fn fetch(pool: &DBPool, id: &Uuid) -> Result<Self, AuthorizeError> {
        let mut connection = pool.get()?;

        let target_user_id = id;

        let principal_and_roles: (models::Principal, models::UserRole) = schema::principal::table
            .filter(schema::principal::user_id.eq(target_user_id))
            .inner_join(schema::user_role::table)
            .first::<(models::Principal, models::UserRole)>(&mut connection)?;

        let user_permissions: Vec<models::UserPermission> =
            models::UserPermission::belonging_to(&principal_and_roles.0)
                .select(schema::user_permission::all_columns)
                .load::<models::UserPermission>(&mut connection)?;

        let user_roles: Vec<(models::RolePermission, models::Permission)> =
            schema::role_permission::table
                .filter(schema::role_permission::role_name.eq(&principal_and_roles.1.role_name))
                .inner_join(schema::permission::table)
                .load::<(models::RolePermission, models::Permission)>(&mut connection)?;

        let mut principal = Principal::default();

        principal.user_id = principal_and_roles.0.user_id;
        principal.allowed_domains = principal_and_roles.0.allowed_domains;
        principal.all_domains_allowed = principal_and_roles.0.all_domains_allowed;

        principal.user_permissions = user_permissions;
        principal.user_roles = user_roles
            .into_iter()
            .map(|(role_permission, permission)| Role {
                name: role_permission.role_name,
                domain: role_permission.domain,
                description: "".into(),
                permissions: vec![models::Permission {
                    permission_name: permission.permission_name,
                    resource_name: permission.resource_name,
                    actions: permission.actions,
                    effect: permission.effect,
                }],
            })
            .collect();
        Ok(principal)
    }
}

impl Default for Principal {
    fn default() -> Self {
        Self {
            user_id: Uuid::nil(),
            allowed_domains: vec![],
            all_domains_allowed: false,
            user_permissions: vec![],
            user_roles: vec![],
        }
    }
}

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Meta {}

impl Meta {
    pub fn new(_authenticator: &RBACAuthorizer) -> Self {
        Self {}
    }
}

#[typetag::serde(name = "rbac_meta")]
impl meta::Meta for Meta {}

pub trait Authorizer: MetaInfo + TypedMetaInfo<Meta> {
    fn is_authorized_by_uid(
        &self,
        pool: DBPool,
        uid: &Uuid,
        permission: &String,
    ) -> Result<(bool, models::ResourceReferenceAttributes), AuthorizeError>;

    fn is_authorized(
        &self,
        pool: DBPool,
        principal: Principal,
        permission: &String,
    ) -> Result<(bool, models::ResourceReferenceAttributes), AuthorizeError>;
}

pub struct RBACAuthorizer {}

impl RBACAuthorizer {
    pub fn new() -> Self {
        Self {}
    }

    pub fn parse_permission_string(
        permission_string: String,
    ) -> Result<(String, models::Permission), AuthorizeError> {
        let splitted = permission_string.split("::").collect::<Vec<&str>>();

        if splitted.len() != 4 {
            return Err(AuthorizeError::WrongSizedPermissionString);
        }

        Ok((
            splitted[0].into(),
            models::Permission {
                permission_name: "".into(),
                resource_name: splitted[1].into(),
                actions: vec![Some(splitted[2].into())],
                effect: splitted[3].into(),
            },
        ))
    }
}

impl Authorizer for RBACAuthorizer {
    #[tracing::instrument(skip(self, pool))]
    fn is_authorized(
        &self,
        pool: DBPool,
        principal: Principal,
        permission_string: &String,
    ) -> Result<(bool, models::ResourceReferenceAttributes), AuthorizeError> {
        let (domain, permission) = Self::parse_permission_string(permission_string.clone())?;

        if !principal.all_domains_allowed
            && !principal.allowed_domains.contains(&Some(domain.clone()))
        {
            return Err(AuthorizeError::UserNotAllowedTOperateOnDomain(
                domain.clone(),
            ));
        }

        let has_principal_user_action = principal
            .user_permissions
            .into_iter()
            .filter(|p| {
                p.domain == domain
                    && permission.resource_name == p.resource_name
                    && p.actions.contains(&permission.actions[0])
                    && p.effect == models::Effect::Allow
            })
            .collect::<Vec<models::UserPermission>>()
            .len()
            > 0;

        let has_role_permission = principal.user_roles.into_iter().fold(false, |acc, role| {
            acc || {
                role.permissions
                    .into_iter()
                    .filter(|p| {
                        role.domain == domain
                            && permission.resource_name == p.resource_name
                            && p.actions.contains(&permission.actions[0])
                            && p.effect == models::Effect::Allow
                    })
                    .collect::<Vec<models::Permission>>()
                    .len()
                    > 0
            }
        });

        if !has_principal_user_action && !has_role_permission {
            return Ok((false, models::ResourceReferenceAttributes::default()));
        }

        if has_principal_user_action {
            debug!("Authorized by user permission");
        } else if has_role_permission {
            debug!("Authorized by role permission");
        }

        // Why should anyone check permissions on a resource he knows he is not allowed to access?
        // Anyhow in this case the user is not allowed to access the resource, so we can just return false.
        if permission.effect == models::Effect::Deny {
            return Ok((false, models::ResourceReferenceAttributes::default()));
        }

        // TODO: error handling
        let resource = Resource::get(pool, &permission.resource_name)
            .map_err(|_e| AuthorizeError::Any(Some("Failed to get resource".into())))?;

        let action = &permission.actions[0]
            .clone()
            .expect("Set by parsing method cannot be None. Only `Option` due to diesel.");

        let domains_and_actions = resource.actions_per_domain(vec![&domain]);

        if !domains_and_actions.contains_key(&domain) {
            return Err(AuthorizeError::ResourceNotExistingInSpecifiedDomain(
                domain.clone(),
                permission.resource_name.clone(),
            ));
        }

        let has_action = domains_and_actions
            .values()
            .flat_map(|v| v.iter())
            .cloned()
            .collect::<HashSet<String>>()
            .contains(action);

        if !has_action {
            return Err(AuthorizeError::ActionNotPossibleOnResource(action.clone()));
        }

        Ok((true, resource.attributes(vec![&domain])))
    }

    #[tracing::instrument(skip(self, pool))]
    fn is_authorized_by_uid(
        &self,
        pool: DBPool,
        uid: &Uuid,
        permission: &String,
    ) -> Result<(bool, models::ResourceReferenceAttributes), AuthorizeError> {
        let principal;
        {
            let span = tracing::info_span!("fetching_principal");
            let _enter = span.enter();
            principal = Principal::user_to_principal(&pool, uid)?;
        }

        self.is_authorized(pool, principal, permission)
    }
}

impl MetaInfo for RBACAuthorizer {
    fn meta(&self) -> Box<dyn meta::Meta> {
        Box::new(Meta::new(self))
    }
}

impl TypedMetaInfo<Meta> for RBACAuthorizer {
    fn concrete_meta(&self) -> Meta {
        Meta::new(self)
    }
}
