use std::path::PathBuf;

use serde::Deserialize;

#[derive(Debug, Deserialize, Clone)]
pub struct Config {
    pub postgres_config: PostgresConfig,
    pub jwt: JWTConfig,
    pub api: APIConfig,
}

#[derive(Debug, Deserialize, Clone)]
pub struct APIConfig {
    pub cookie: CookieConfig,
}

#[derive(Debug, Deserialize, Clone)]
pub struct CookieConfig {
    pub domain: String,
    pub path: String,
    pub secure: bool,
    pub http_only: bool,
    pub max_age_minutes: i64,
}

#[derive(Debug, Deserialize, Clone)]
pub struct PostgresConfig {
    pub database: String,
    pub host: String,
    #[serde(default = "_default_port")]
    pub port: u16,
    pub username: String,
    pub password: String,
    #[serde(default = "_default_type")]
    pub password_type: PostgresPasswordType,
    #[serde(default = "_default_pool_size")]
    pub pool_size: u32,
}

#[derive(Debug, Deserialize, Clone)]
pub enum PostgresPasswordType {
    #[serde(rename = "env")]
    Env,
    #[serde(rename = "file")]
    File,
    #[serde(rename = "value")]
    Value,
}

const fn _default_type() -> PostgresPasswordType {
    PostgresPasswordType::Value
}

const fn _default_pool_size() -> u32 {
    20
}

const fn _default_port() -> u16 {
    5432
}

#[derive(Debug, Deserialize, Clone)]
pub struct JWTConfig {
    pub use_mode: JWTSecretModeOptions,
    pub secret_mode: Vec<JWTSecretMode>,
}

#[derive(Debug, Deserialize, Clone, PartialEq)]
#[serde(rename_all = "lowercase")]
pub enum JWTSecretModeOptions {
    ED25519,
    HS512,
}

#[derive(Debug, Deserialize, Clone)]
#[serde(rename_all = "lowercase", tag = "name")]
pub enum JWTSecretMode {
    ED25519(ED25519Config),
    HS512(HS512Config),
}

#[derive(Debug, Deserialize, Clone, Default)]
pub struct HS512Config {
    pub secret: String,
}

#[derive(Debug, Deserialize, Clone, Default)]

pub struct ED25519Config {
    pub access_token_private_key: PathBuf,
    pub refresh_token_private_key: PathBuf,
    #[serde(default = "bool::default")]
    pub watch: bool,
}
