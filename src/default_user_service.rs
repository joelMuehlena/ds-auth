use async_trait::async_trait;
use diesel::{
    pg,
    r2d2::{ConnectionManager, Pool},
    ExpressionMethods, QueryDsl, RunQueryDsl,
};

use crate::{
    error::IAMError,
    meta::{self, MetaInfo},
    models::{self, User},
    schema::user::dsl,
    user_service::{Meta, UserService, UserServiceError},
};

pub struct PostgresUserService {
    pool: Pool<ConnectionManager<pg::PgConnection>>,
}

impl PostgresUserService {
    pub fn new(pool: Pool<ConnectionManager<pg::PgConnection>>) -> Result<Self, IAMError> {
        Ok(PostgresUserService { pool })
    }
}

#[async_trait]
impl UserService for PostgresUserService {
    async fn get_user(&self, email: &String) -> Result<User, UserServiceError> {
        let mut conn = self.pool.get().expect("Pool not acquired");

        let user = dsl::user
            .filter(dsl::email.eq(email))
            .load::<models::User>(&mut conn)
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        let user = match user.first() {
            Some(u) => u.clone(),
            None => return Err(UserServiceError::UserNotFound),
        };

        Ok(user)
    }

    async fn get_user_by_id(&self, uid: uuid::Uuid) -> Result<User, UserServiceError> {
        let mut conn = self.pool.get().expect("Pool not acquired");

        let user: models::User = dsl::user
            .find(uid)
            .first(&mut conn)
            .map_err(|e| UserServiceError::FailedToFetchUser(Box::from(e)))?;

        Ok(user)
    }
}

impl MetaInfo for PostgresUserService {
    fn meta(&self) -> Box<dyn meta::Meta> {
        Box::new(Meta {})
    }
}
