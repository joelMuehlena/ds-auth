use thiserror::Error;

#[derive(Debug, Error)]
pub enum IAMError {
    #[error("Authorization error: {0}")]
    AuthorizeError(Box<dyn std::error::Error>),
    #[error("DB error: {0}")]
    DBError(Box<dyn std::error::Error>),
    #[error("Config error: {0}")]
    ConfigError(String),
    #[error("JWT error: {0}")]
    JWTError(#[from] crate::jwt::Error),
    #[error("Unspecific error: {0}")]
    Any(#[from] anyhow::Error),
    #[error("Error with metrics: {0}")]
    MetricsError(anyhow::Error),
}
