use std::sync::{atomic::AtomicBool, Arc, RwLock};

use chrono::Duration;
use diesel::{
    r2d2::{ConnectionManager, Pool},
    PgConnection,
};
use serde::{Deserialize, Serialize};
use tracing::{debug, info};

use tonic::transport::Server as GRPCServer;

use crate::{
    auth_type::AuthType,
    authorization,
    config::{Config, JWTSecretMode, JWTSecretModeOptions, PostgresConfig},
    default_user_service::PostgresUserService,
    error::IAMError,
    jwt::{self, AccessClaims, JWT},
    meta::{self, MetaInfo, TypedMetaInfo},
    metrics, models,
    provider::{EmailProvider, Provider, ProviderData},
    resource::{self, resource_manager::resource_manager::resource_manager_server},
    user_service::UserService,
};

pub type DBPool = Pool<ConnectionManager<PgConnection>>;

pub type IAMLock = Arc<RwLock<IAM>>;

pub struct IAM {
    pub authorizer: Box<dyn authorization::Authorizer + Sync + Send>,
    pub state: AtomicBool,
    jwt: Arc<JWT>,
    user_service: Arc<dyn UserService + Sync + Send>,
    pool: DBPool,
    pub config: Config,
    pub metrics: metrics::MetricsProvider,
}

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Meta {
    authorizer: authorization::Meta,
    jwt: jwt::Meta,
}

impl Meta {
    pub fn new(iam: &IAM) -> Self {
        Self {
            authorizer: iam.authorizer.concrete_meta(),
            jwt: iam.jwt.concrete_meta(),
        }
    }
}

mod proto {
    pub(crate) const FILE_DESCRIPTOR_SET: &[u8] =
        tonic::include_file_descriptor_set!("authserver_proto_descriptor");
}

impl IAM {
    pub fn new(config: Config) -> Result<Self, IAMError> {
        let pool = Self::create_db_pool(&config.postgres_config)?;

        let user_service = PostgresUserService::new(pool.clone())?;
        let mut iam = Self::new_with_user_service(config, Arc::new(user_service))?;

        drop(iam.pool);

        iam.pool = pool;

        Ok(iam)
    }

    pub fn new_with_user_service(
        config: Config,
        user_service: Arc<dyn UserService + Sync + Send>,
    ) -> Result<Self, IAMError> {
        let active_secret_mode = config.jwt.secret_mode.iter().find(|mode| match mode {
            JWTSecretMode::ED25519(_) if config.jwt.use_mode == JWTSecretModeOptions::ED25519 => {
                true
            }
            JWTSecretMode::HS512(_) if config.jwt.use_mode == JWTSecretModeOptions::HS512 => true,
            _ => false,
        });

        let active_secret_mode = match active_secret_mode {
            None => {
                return Err(IAMError::ConfigError(String::from(
                    "No JWT secret mode provided",
                )))
            }
            Some(mode) => mode,
        };

        let jwt = JWT::new(active_secret_mode.clone()).map_err(|err| IAMError::JWTError(err))?;

        let pool = Self::create_db_pool(&config.postgres_config)?;

        let metrics_provider = metrics::MetricsProvider::new();

        metrics_provider
            .registry
            .write()
            .map_err(|e| {
                IAMError::MetricsError(
                    anyhow::anyhow!("Failed to register process collector: {:?}", e)
                        .context("Failed to write metrics registry (RwLock)"),
                )
            })?
            .register_collector(Box::new(metrics::ProcessCollector::new()));

        let iam = IAM {
            authorizer: Box::new(authorization::RBACAuthorizer::new()),
            state: AtomicBool::new(true),
            jwt: Arc::new(jwt),
            user_service,
            pool,
            config,
            metrics: metrics_provider,
        };

        Ok(iam)
    }

    pub async fn init(&self) {
        info!("Initializing IAM");
        // TODO: secure grpc with api keys -> needs api key implementation

        let addr = "0.0.0.0:50051".parse().unwrap();

        let reflection_service = tonic_reflection::server::Builder::configure()
            .register_encoded_file_descriptor_set(proto::FILE_DESCRIPTOR_SET)
            .build()
            .unwrap();

        let (mut health_reporter, health_service) = tonic_health::server::health_reporter();
        health_reporter
            .set_serving::<resource_manager_server::ResourceManagerServer<resource::ResourceManager>>()
            .await;

        // TODO: make stoppable and controllable
        // E.g. reload, stop, start, etc.
        // https://docs.rs/tokio/latest/tokio/signal/fn.ctrl_c.html
        // https://docs.rs/tokio-util/latest/tokio_util/sync/struct.CancellationToken.html
        // Create a nice main loop that can be controlled with the calling application
        tokio::spawn(
            GRPCServer::builder()
                .add_service(reflection_service)
                .add_service(health_service)
                .add_service(resource_manager_server::ResourceManagerServer::new(
                    resource::ResourceManager::new(self.pool.clone()),
                ))
                .serve(addr),
        );
    }

    fn create_db_pool(config: &PostgresConfig) -> Result<DBPool, IAMError> {
        let manager = ConnectionManager::<PgConnection>::new(format!(
            "postgres://{}:{}@{}:{}/{}",
            config.username, config.password, config.host, config.port, config.database
        ));

        let pool = Pool::builder()
            .max_size(config.pool_size)
            .build(manager)
            .map_err(|e| IAMError::DBError(Box::from(e)))?;

        Ok(pool)
    }

    pub async fn authorize(
        &self,
        provider_type: AuthType,
        data: ProviderData,
    ) -> Result<(String, String), IAMError> {
        let provider: Box<dyn Provider> = match provider_type {
            AuthType::Email => match EmailProvider::parse_data(data) {
                Ok(p) => Ok(Box::from(p)),
                Err(e) => Err(e),
            },
        }
        .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        let auth_result = provider
            .authorize(self.user_service.clone())
            .await
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        debug!("Auth result: {:?}", auth_result);

        let access_token = self
            .jwt
            .create_access_token(auth_result.user.clone())
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        let refresh_token = self
            .jwt
            .create_refresh_token(auth_result.user)
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        Ok((access_token, refresh_token))
    }

    pub async fn refresh(&self, token: String) -> Result<(String, String), IAMError> {
        let uid = self
            .jwt
            .verify_refresh_token(token)
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        let user = self
            .user_service
            .get_user_by_id(uid)
            .await
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        let access_token = self
            .jwt
            .create_access_token(user.clone())
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        let refresh_token = self
            .jwt
            .create_refresh_token(user)
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        Ok((access_token, refresh_token))
    }

    pub fn verify(&self, token: String) -> Result<AccessClaims, IAMError> {
        let user_claims = self
            .jwt
            .verify_access_token(token)
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        Ok(user_claims)
    }

    pub fn is_authorized(
        &self,
        token: String,
        permission: String,
    ) -> Result<(bool, models::ResourceReferenceAttributes), IAMError> {
        let user_claims = self.verify(token)?;

        let authorization_result = self
            .authorizer
            .is_authorized_by_uid(self.pool.clone(), &user_claims.sub, &permission)
            .map_err(|e| IAMError::AuthorizeError(Box::from(e)))?;

        debug!(
            "Checking permission for user {}: {:?}; Is authorized: {:?}",
            &user_claims.sub, permission, authorization_result.0
        );

        Ok(authorization_result)
    }

    pub fn token_lifetimes(&self) -> (Duration, Duration) {
        (
            self.jwt.get_access_token_lifetime(),
            self.jwt.get_refresh_token_lifetime(),
        )
    }
}

#[typetag::serde(name = "iam_meta")]
impl meta::Meta for Meta {}

impl MetaInfo for IAM {
    fn meta(&self) -> Box<dyn meta::Meta> {
        Box::new(Meta::new(self))
    }
}

impl TypedMetaInfo<Meta> for IAM {
    fn concrete_meta(&self) -> Meta {
        Meta::new(self)
    }
}
