use serde::{Deserialize, Serialize};
use uuid::Uuid;

#[derive(Debug, Serialize, Deserialize)]
pub struct AccessClaims {
    pub sub: Uuid,
    pub iss: String,
    pub exp: i64,
    pub iat: u64,
    pub given_name: String,
    pub family_name: String,
    pub email: String,
    pub picture: Option<String>,
    pub preferred_username: String,
    pub locale: Option<String>,
}

#[derive(Debug, Serialize, Deserialize)]
pub struct RefreshClaims {
    pub sub: Uuid,
    pub iss: String,
    pub exp: i64,
    pub iat: u64,
    pub jti: Uuid,
}
