use std::{
    fs,
    path::PathBuf,
    sync::{Arc, RwLock},
    time,
};

use asn1_rs::{Oid, ToDer};
use base64::{engine::general_purpose, Engine as _};

use chrono::{Duration, Utc};
use jsonwebtoken::{decode, encode, get_current_timestamp, DecodingKey, EncodingKey, Validation};
use notify::RecursiveMode;
use notify_debouncer_mini::{new_debouncer, DebounceEventResult};
use ring::signature::{self, KeyPair};
use serde::{Deserialize, Serialize};
use tokio::sync::mpsc;
use tracing::{debug, error};

use crate::{
    config::JWTSecretMode,
    jwt::Error,
    meta::{self, MetaInfo},
    models::User,
};

use super::{AccessClaims, Mode, RefreshClaims};

use crate::__read_jwt_mutex__ as read_jwt_mutex;

#[rustfmt::skip]
const ED25519_OID: Oid<'static> = asn1_rs::oid!(1.3.101.112);

#[derive(Debug)]
pub struct Ed25519KeyPair {
    pub key_pair: signature::Ed25519KeyPair,
    raw: Vec<u8>,
}

#[derive(Debug, Clone)]
pub struct ED25519Mode {
    access_key_token_path: PathBuf,
    refresh_key_token_path: PathBuf,
    access_key_pair: Arc<RwLock<Ed25519KeyPair>>,
    refresh_key_pair: Arc<RwLock<Ed25519KeyPair>>,
}

impl ED25519Mode {
    pub fn new(
        access_key_token_path: PathBuf,
        refresh_key_token_path: PathBuf,
        watch: bool,
    ) -> Result<ED25519Mode, Error> {
        debug!(
            "JWT initiated (watching: {:?}): {:?}; {:?}",
            watch, access_key_token_path, refresh_key_token_path
        );

        if access_key_token_path == refresh_key_token_path {
            return Err(Error::KeyLoadError(std::io::Error::new(
                std::io::ErrorKind::Other,
                "Access and refresh key file paths must be different",
            )));
        }

        // Create a default set of keys for initializing the values of Self
        let (access_dummy, refresh_dummy) = Self::create_dummy_key_pair();

        let mut mode = ED25519Mode {
            access_key_token_path,
            refresh_key_token_path,
            access_key_pair: Arc::new(RwLock::new(Ed25519KeyPair {
                key_pair: access_dummy,
                raw: Vec::new(),
            })),
            refresh_key_pair: Arc::new(RwLock::new(Ed25519KeyPair {
                key_pair: refresh_dummy,
                raw: Vec::new(),
            })),
        };

        mode.load_keys()?;

        let mut jwt_watcher = mode.clone();
        if watch {
            tokio::task::spawn(async move {
                match jwt_watcher.watch().await {
                    Ok(_) => debug!("JWT watch task exited successfully"),
                    Err(e) => error!("JWT watch task exited with error: {:?}", e),
                };
            });
        }

        Ok(mode)
    }

    fn create_dummy_key_pair() -> (signature::Ed25519KeyPair, signature::Ed25519KeyPair) {
        let doc =
            signature::Ed25519KeyPair::generate_pkcs8(&ring::rand::SystemRandom::new()).unwrap();
        let ac_pair = signature::Ed25519KeyPair::from_pkcs8(doc.as_ref()).unwrap();
        let rf_pair = signature::Ed25519KeyPair::from_pkcs8(doc.as_ref()).unwrap();

        (ac_pair, rf_pair)
    }

    pub async fn watch(&mut self) -> notify::Result<()> {
        let (tx, mut rx) = mpsc::channel(256);

        let token_paths = (
            self.access_key_token_path.clone(),
            self.refresh_key_token_path.clone(),
        );

        let mut debouncer = new_debouncer(
            time::Duration::from_secs_f32(0.5),
            move |res: DebounceEventResult| match res {
                Ok(events) => {
                    debug!("event: {:?}", events);

                    events
                          .into_iter()
                          .filter(|e| e.path ==  token_paths.0 || e.path == token_paths.1)
                          .for_each(|event| {
                            if !event.path.exists() {
                              error!("Key path ({:?}) does not exist. Maybe, the event was part of a delete event. Skipping this action.", event.path)
                            } else if let Err(err) = tx.blocking_send(event.path) {
                              error!("Error handling file (sending from debouncer to handler) {:#?}", err);
                            };
                        })
                }
                Err(e) => {
                    error!("Error watching key files (debouncer): {:?}", e);
                }
            },
        )?;

        if self.access_key_token_path.parent().is_none()
            || self.refresh_key_token_path.parent().is_none()
        {
            return Err(notify::Error::generic(
                "Parent directory of key files must be specified",
            ));
        }

        if self.access_key_token_path.parent() == self.refresh_key_token_path.parent() {
            debug!(
                "Registering single watcher for token base: {:?}",
                self.access_key_token_path.parent()
            );
            debouncer.watcher().watch(
                &self.access_key_token_path.parent().unwrap(), // Unwrapping is ok here because none case was checked before
                RecursiveMode::NonRecursive,
            )?;
        } else {
            debug!(
                "Registering two watcher for token bases: ({:?}); ({:?})",
                self.access_key_token_path.parent().unwrap(),
                self.access_key_token_path.parent().unwrap()
            );
            for path in [&self.access_key_token_path, &self.refresh_key_token_path].iter() {
                debouncer
                    .watcher()
                    .watch(path.parent().unwrap(), RecursiveMode::NonRecursive)?;
            }
        }

        while let Some(file) = rx.recv().await {
            debug!("Received change event for JWT key file: {:?}", file);
            if let Err(e) = self.load_keys() {
                error!("Failed to reload JWT Keys: {:?}", e);
                return Err(notify::Error::generic("Loading keys failed"));
            };
        }

        Ok(())
    }

    #[tracing::instrument(skip(self))]
    pub fn load_keys(&mut self) -> Result<(), Error> {
        debug!("Load JWT keys");

        for path in [&self.access_key_token_path, &self.refresh_key_token_path].iter() {
            let key_content = fs::read(path).map_err(|e| Error::KeyLoadError(e))?;
            let key_pair =
                ring::signature::Ed25519KeyPair::from_pkcs8_maybe_unchecked(key_content.as_slice())
                    .map_err(|e| Error::KeyParseError(e))?;

            let key_pair = Ed25519KeyPair {
                key_pair,
                raw: key_content,
            };

            let loading_key_pair: &Arc<RwLock<Ed25519KeyPair>>;
            if *path == &self.access_key_token_path {
                loading_key_pair = &self.access_key_pair;
            } else if *path == &self.refresh_key_token_path {
                loading_key_pair = &self.refresh_key_pair;
            } else {
                error!(
                    "Unknown key file path: {:?}. Expected {:?} or {:?}",
                    path, self.access_key_token_path, self.refresh_key_token_path,
                );
                return Err(Error::KeyLoadError(std::io::Error::new(
                    std::io::ErrorKind::Other,
                    "Unknown key file path",
                )));
            }

            match loading_key_pair.write() {
                Ok(mut loading_key_pair) => {
                    loading_key_pair.key_pair = key_pair.key_pair;
                    loading_key_pair.raw = key_pair.raw;
                }
                Err(e) => {
                    error!("Failed to write access key pair: {:?}", e);
                    return Err(Error::KeyRWError(e.to_string()));
                }
            }
        }

        Ok(())
    }
}

impl Mode for ED25519Mode {
    #[tracing::instrument(skip(self, user))]
    fn create_access_token(&self, user: User, lifetime: Duration) -> Result<String, Error> {
        let mut now = Utc::now();
        now = now.checked_add_signed(lifetime).unwrap();

        let claims = AccessClaims {
            email: user.email,
            preferred_username: user.username,
            family_name: user.lastname,
            given_name: user.firstname,
            picture: user.picture,
            locale: user.locale,
            iss: "ds-auth::iam".to_string(),
            sub: user.id,
            iat: get_current_timestamp(),
            exp: now.timestamp(),
        };

        let key_pair = read_jwt_mutex!(self.access_key_pair)?;

        let encoding_key = EncodingKey::from_ed_der(key_pair.raw.as_slice());
        drop(key_pair); // Release lock explicitly

        let token = encode(
            &jsonwebtoken::Header::new(jsonwebtoken::Algorithm::EdDSA),
            &claims,
            &encoding_key,
        )
        .map_err(|e| Error::TokenError(e))?;

        Ok(token)
    }

    #[tracing::instrument(skip(self, user))]
    fn create_refresh_token(&self, user: User, lifetime: Duration) -> Result<String, Error> {
        let mut now = Utc::now();
        now = now.checked_add_signed(lifetime).unwrap();

        let claims = RefreshClaims {
            iss: "ds-auth::iam".to_string(),
            sub: user.id,
            iat: get_current_timestamp(),
            exp: now.timestamp(),
            jti: uuid::Uuid::new_v4(),
        };

        let key_pair = read_jwt_mutex!(self.refresh_key_pair)?;

        let encoding_key = EncodingKey::from_ed_der(key_pair.raw.as_slice());

        let token = encode(
            &jsonwebtoken::Header::new(jsonwebtoken::Algorithm::EdDSA),
            &claims,
            &encoding_key,
        )
        .map_err(|e| Error::TokenError(e))?;

        Ok(token)
    }

    #[allow(dead_code)]
    fn revoke_refresh_token(&self) {
        todo!()
    }

    #[tracing::instrument(skip(self, token))]
    fn verify_refresh_token(&self, token: String) -> Result<uuid::Uuid, Error> {
        let validation = Validation::new(jsonwebtoken::Algorithm::EdDSA);

        let key_pair = read_jwt_mutex!(self.refresh_key_pair)?;

        let token = decode::<RefreshClaims>(
            &token,
            &DecodingKey::from_ed_der(key_pair.key_pair.public_key().as_ref()),
            &validation,
        )
        .map_err(|e| Error::TokenError(e))?
        .claims;

        Ok(token.sub)
    }

    #[tracing::instrument(skip(self, token))]
    fn verify_access_token(&self, token: String) -> Result<AccessClaims, Error> {
        let validation = Validation::new(jsonwebtoken::Algorithm::EdDSA);

        let key_pair = read_jwt_mutex!(self.access_key_pair)?;

        let token = decode::<AccessClaims>(
            &token,
            &DecodingKey::from_ed_der(key_pair.key_pair.public_key().as_ref()),
            &validation,
        )
        .map_err(|e| Error::TokenError(e))?
        .claims;

        Ok(token)
    }

    fn name(&self) -> &'static str {
        "ED25519"
    }

    fn mode(&self) -> JWTSecretMode {
        JWTSecretMode::ED25519(crate::config::ED25519Config::default())
    }
}

#[derive(Debug, Serialize, Clone, Deserialize)]
struct ED25519ModeMeta {
    access_key_public_key: Vec<u8>,
    access_key_public_key_pem: String,
    refresh_key_public_key: Vec<u8>,
    refresh_key_public_key_pem: String,
}

#[typetag::serde(name = "ed25519_meta")]
impl meta::Meta for ED25519ModeMeta {}

impl MetaInfo for ED25519Mode {
    fn meta(&self) -> Box<dyn meta::Meta> {
        let access_key_public_key = read_jwt_mutex!(self.access_key_pair)
            .expect("Failed to read access key pair")
            .key_pair
            .public_key()
            .as_ref()
            .to_owned();

        let refresh_key_public_key = read_jwt_mutex!(self.refresh_key_pair)
            .expect("Failed to read refresh key pair")
            .key_pair
            .public_key()
            .as_ref()
            .to_owned();

        let mut oid_sequence = Vec::new();
        let _ = ED25519_OID.write_der(&mut oid_sequence);
        let oid_sequence = asn1_rs::Sequence::new(oid_sequence.into());

        let access_key_bit_string = asn1_rs::BitString::new(0, &access_key_public_key);
        let refresh_key_bit_string = asn1_rs::BitString::new(0, &refresh_key_public_key);

        let (mut der_access_key_sequence, mut der_refresh_key_sequence) = (Vec::new(), Vec::new());

        let _ = oid_sequence.write_der(&mut der_access_key_sequence);
        let _ = access_key_bit_string.write_der(&mut der_access_key_sequence);

        let _ = oid_sequence.write_der(&mut der_refresh_key_sequence);
        let _ = refresh_key_bit_string.write_der(&mut der_refresh_key_sequence);

        let (der_access_key_sequence, der_refresh_key_sequence) = (
            asn1_rs::Sequence::new(der_access_key_sequence.into()),
            asn1_rs::Sequence::new(der_refresh_key_sequence.into()),
        );

        let der_access_key_sequence = der_access_key_sequence
            .to_der_vec()
            .expect("Parsing DER failed");
        let der_refresh_key_sequence = der_refresh_key_sequence
            .to_der_vec()
            .expect("Parsing DER failed");

        Box::new(ED25519ModeMeta {
            access_key_public_key_pem: der_to_pem(&der_access_key_sequence).unwrap(),
            refresh_key_public_key_pem: der_to_pem(&der_refresh_key_sequence).unwrap(),
            access_key_public_key: der_access_key_sequence,
            refresh_key_public_key: der_refresh_key_sequence,
        })
    }
}

fn der_to_pem(der_bytes: &Vec<u8>) -> Result<String, Box<dyn std::error::Error>> {
    // Create the PEM content
    let mut pem_content = String::from("-----BEGIN PUBLIC KEY-----\n");
    pem_content += general_purpose::STANDARD.encode(der_bytes).as_str();
    pem_content += "\n-----END PUBLIC KEY-----\n";

    Ok(pem_content)
}
