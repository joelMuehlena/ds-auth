use jsonwebtoken::errors::Error as JWTError;
use ring::error::KeyRejected;
use std::io::Error as IOError;

#[derive(Debug)]
pub enum Error {
    KeyLoadError(IOError),
    KeyParseError(KeyRejected),
    KeyRWError(String),
    TokenError(JWTError),
}

impl std::fmt::Display for Error {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            Error::KeyLoadError(e) => write!(f, "failed to load a key from disk: {:?}", e),
            Error::KeyParseError(e) => write!(f, "Key could not be parsed: {:?}", e),
            Error::TokenError(e) => write!(f, "JWT token error: {:?}", e),
            Error::KeyRWError(e) => write!(f, "Key read/write error: {:?}", e),
        }
    }
}

impl std::error::Error for Error {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            Error::KeyLoadError(ref e) => Some(e),
            Error::KeyParseError(ref e) => Some(e),
            Error::TokenError(ref e) => Some(e),
            Error::KeyRWError(_) => None,
        }
    }
}
