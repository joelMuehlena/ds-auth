#![allow(unused_variables)]

use serde::{Deserialize, Serialize};

use super::Mode;
use crate::{
    config::JWTSecretMode,
    jwt::Error,
    meta::{self, MetaInfo},
};

#[derive(Debug, Clone)]
pub struct HS512Mode {
    secret: String,
}

impl HS512Mode {
    pub fn new(secret: String) -> Result<HS512Mode, Error> {
        Ok(HS512Mode { secret })
    }
}

impl Mode for HS512Mode {
    fn revoke_refresh_token(&self) {
        todo!()
    }

    fn create_access_token(
        &self,
        user: crate::models::User,
        lifetime: chrono::Duration,
    ) -> Result<String, super::Error> {
        todo!()
    }

    fn create_refresh_token(
        &self,
        user: crate::models::User,
        lifetime: chrono::Duration,
    ) -> Result<String, super::Error> {
        todo!()
    }

    fn verify_access_token(&self, token: String) -> Result<super::AccessClaims, super::Error> {
        todo!()
    }

    fn verify_refresh_token(&self, token: String) -> Result<uuid::Uuid, super::Error> {
        todo!()
    }

    fn name(&self) -> &'static str {
        "HS512"
    }

    fn mode(&self) -> JWTSecretMode {
        JWTSecretMode::HS512(crate::config::HS512Config::default())
    }
}

#[derive(Debug, Serialize, Clone, Deserialize)]
struct HS512ModeMeta {}

#[typetag::serde(name = "hs512_meta")]
impl meta::Meta for HS512ModeMeta {}

impl MetaInfo for HS512Mode {
    fn meta(&self) -> Box<dyn meta::Meta> {
        Box::new(HS512ModeMeta {})
    }
}
