use chrono::Duration;
use serde::{Deserialize, Serialize};
use uuid::Uuid;

use crate::{
    config::JWTSecretMode,
    jwt::{claims::AccessClaims, error::Error},
    meta::{self, MetaInfo, TypedMetaInfo},
};

use crate::models::User;

use super::{ED25519Mode, HS512Mode, Mode};

pub const DEFAULT_JWT_REFRESH_TOKEN_LIFETIME_MINUTES: i64 = 7200;
// pub const DEFAULT_JWT_ACCESS_TOKEN_LIFETIME_MINUTES: i64 = 5;
pub const DEFAULT_JWT_ACCESS_TOKEN_LIFETIME_MINUTES: i64 = 40;

#[derive(Debug, Clone)]
pub struct JWT {
    refresh_token_lifetime: Duration,
    access_token_lifetime: Duration,
    mode: Box<dyn Mode>,
}

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Meta {
    mode: String,
    jwt: Box<dyn meta::Meta>,
    access_token_lifetime: i64,
    refresh_token_lifetime: i64,
}

impl Meta {
    pub fn new(jwt: &JWT) -> Self {
        Self {
            mode: jwt.mode.name().to_string(),
            jwt: jwt.mode.meta(),
            access_token_lifetime: jwt.access_token_lifetime.num_minutes(),
            refresh_token_lifetime: jwt.refresh_token_lifetime.num_minutes(),
        }
    }
}

impl JWT {
    pub fn new_with_lifetime(
        mode: JWTSecretMode,
        refresh_token_lifetime: Duration,
        access_token_lifetime: Duration,
    ) -> Result<Self, Error> {
        let mut jwt = JWT::new(mode)?;
        jwt.refresh_token_lifetime = refresh_token_lifetime;
        jwt.access_token_lifetime = access_token_lifetime;
        Ok(jwt)
    }

    pub fn new(mode: JWTSecretMode) -> Result<Self, Error> {
        let mode: Box<dyn Mode> = match mode {
            JWTSecretMode::ED25519(ref mode) => Box::new(ED25519Mode::new(
                mode.access_token_private_key.clone(),
                mode.refresh_token_private_key.clone(),
                mode.watch,
            )?),
            JWTSecretMode::HS512(ref mode) => Box::new(HS512Mode::new(mode.secret.clone())?),
        };

        let jwt = JWT {
            access_token_lifetime: Duration::minutes(DEFAULT_JWT_ACCESS_TOKEN_LIFETIME_MINUTES),
            refresh_token_lifetime: Duration::minutes(DEFAULT_JWT_REFRESH_TOKEN_LIFETIME_MINUTES),
            mode,
        };

        Ok(jwt)
    }

    pub fn get_refresh_token_lifetime(&self) -> Duration {
        self.refresh_token_lifetime
    }

    pub fn get_access_token_lifetime(&self) -> Duration {
        self.access_token_lifetime
    }

    pub fn revoke_refresh_token(&self) {
        self.mode.revoke_refresh_token()
    }

    pub fn create_access_token(&self, user: User) -> Result<String, Error> {
        self.mode
            .create_access_token(user, self.access_token_lifetime)
    }

    pub fn create_refresh_token(&self, user: User) -> Result<String, Error> {
        self.mode
            .create_refresh_token(user, self.refresh_token_lifetime)
    }

    pub fn verify_access_token(&self, token: String) -> Result<AccessClaims, Error> {
        self.mode.verify_access_token(token)
    }

    pub fn verify_refresh_token(&self, token: String) -> Result<Uuid, Error> {
        self.mode.verify_refresh_token(token)
    }
}

#[typetag::serde(name = "jwt_meta")]
impl meta::Meta for Meta {}

impl MetaInfo for JWT {
    fn meta(&self) -> Box<dyn meta::Meta> {
        Box::new(Meta::new(self))
    }
}

impl TypedMetaInfo<Meta> for JWT {
    fn concrete_meta(&self) -> Meta {
        Meta::new(self)
    }
}
