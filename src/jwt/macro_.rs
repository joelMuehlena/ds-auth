#[doc(hidden)]
#[macro_export]
macro_rules! __read_jwt_mutex__ {
    ($key_var:expr) => {
        match $key_var.read() {
            Ok(key_pair) => Ok(key_pair),
            Err(e) => Err(Error::KeyRWError(e.to_string())),
        }
    };
}
