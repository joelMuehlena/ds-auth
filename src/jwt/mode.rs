use std::fmt::Debug;

use chrono::Duration;
use uuid::Uuid;

use crate::{config::JWTSecretMode, meta::MetaInfo, models::User};

use super::{error::Error, AccessClaims};

pub trait CloneMode {
    fn clone_box(&self) -> Box<dyn Mode>;
}

impl<T> CloneMode for T
where
    T: 'static + Mode + Clone,
{
    fn clone_box(&self) -> Box<dyn Mode> {
        Box::new(self.clone())
    }
}
pub trait Mode: Debug + CloneMode + Send + Sync + MetaInfo {
    fn revoke_refresh_token(&self);
    fn create_access_token(&self, user: User, lifetime: Duration) -> Result<String, Error>;
    fn create_refresh_token(&self, user: User, lifetime: Duration) -> Result<String, Error>;
    fn verify_access_token(&self, token: String) -> Result<AccessClaims, Error>;
    fn verify_refresh_token(&self, token: String) -> Result<Uuid, Error>;

    fn name(&self) -> &'static str;
    fn mode(&self) -> JWTSecretMode;
}

impl Clone for Box<dyn Mode> {
    fn clone(&self) -> Box<dyn Mode> {
        self.clone_box()
    }
}
