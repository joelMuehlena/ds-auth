use dyn_clone::DynClone;
use std::{any::Any, fmt::Debug};

pub trait MetaToAny: 'static {
    fn as_any(&self) -> &dyn Any;
}

impl<T: 'static> MetaToAny for T {
    fn as_any(&self) -> &dyn Any {
        self
    }
}

#[typetag::serde(tag = "type")]
pub trait Meta: Debug + DynClone {}
dyn_clone::clone_trait_object!(Meta);

pub trait MetaInfo: MetaToAny {
    fn meta(&self) -> Box<dyn Meta>;
}

pub trait TypedMetaInfo<T>: MetaInfo
where
    T: Meta + Clone + 'static,
{
    fn concrete_meta(&self) -> T;
}
