use actix_web::{web, HttpResponse, Result};
use prometheus_client::encoding::text::encode;

use super::MetricsProvider;

pub async fn metrics_handler(state: web::Data<MetricsProvider>) -> Result<HttpResponse> {
    let reg = match state.registry.read() {
        Ok(reg) => reg,
        Err(_) => {
            return Ok(HttpResponse::InternalServerError()
                .content_type("text/plain")
                .body("Failed to read metrics registry"))
        }
    };

    let mut body = String::new();
    encode(&mut body, &reg).unwrap();
    Ok(HttpResponse::Ok()
        .content_type("application/openmetrics-text; version=1.0.0; charset=utf-8")
        .body(body))
}
