use lazy_static::lazy_static;
use prometheus_client::{
    collector::Collector,
    encoding::EncodeMetric,
    metrics::{
        counter::Counter,
        gauge::{ConstGauge, Gauge},
    },
    registry::{Registry, Unit},
};
use std::sync::{atomic::AtomicI64, Arc, RwLock};

lazy_static! {
    // getconf CLK_TCK
    static ref CLK_TCK: i64 = {
        unsafe {
            libc::sysconf(libc::_SC_CLK_TCK)
        }.into()
    };

    // getconf PAGESIZE
    static ref PAGESIZE: i64 = {
        unsafe {
            libc::sysconf(libc::_SC_PAGESIZE)
        }.into()
    };
}

#[derive(Debug, Clone)]
pub struct MetricsProvider {
    pub registry: Arc<RwLock<Registry>>,
}

impl MetricsProvider {
    pub fn new() -> Self {
        Self {
            registry: Arc::new(RwLock::new(Registry::default())),
        }
    }
}

#[derive(Debug)]
pub struct ProcessCollector {
    pid: u32,
    open_fds: Gauge<i64, AtomicI64>,
    max_fds: Gauge<i64, AtomicI64>,
    cpu_total: Counter,
    vsize: Gauge<i64, AtomicI64>,
    rss: Gauge<i64, AtomicI64>,
    start_time: ConstGauge<i64>,
    threads: Gauge<i64, AtomicI64>,
}

impl ProcessCollector {
    pub fn new() -> Self {
        let start_time;

        if let Ok(boot_time) = procfs::boot_time_secs() {
            if let Ok(stat) = procfs::process::Process::myself().and_then(|p| p.stat()) {
                start_time = ConstGauge::new(stat.starttime as i64 / *CLK_TCK + boot_time as i64);
            } else {
                start_time = ConstGauge::default();
            }
        } else {
            start_time = ConstGauge::default();
        }

        Self {
            pid: std::process::id(),
            open_fds: Gauge::default(),
            max_fds: Gauge::default(),
            cpu_total: Counter::default(),
            vsize: Gauge::default(),
            rss: Gauge::default(),
            start_time,
            threads: Gauge::default(),
        }
    }

    fn process_collector(&self) {
        let p = match procfs::process::Process::new(self.pid.try_into().expect("pid is not i32")) {
            Ok(p) => p,
            Err(e) => {
                // we can't construct a Process object, so there's no stats to gather
                panic!("Error in process_collector: {:?}", e);
            }
        };

        if let Ok(fd_count) = p.fd_count() {
            self.open_fds.set(fd_count as i64);
        }

        if let Ok(limits) = p.limits() {
            if let procfs::process::LimitValue::Value(max) = limits.max_open_files.soft_limit {
                self.max_fds.set(max as i64);
            }
        }

        if let Ok(stat) = p.stat() {
            // memory
            self.vsize.set(stat.vsize as i64);
            self.rss.set((stat.rss as i64) * *PAGESIZE);

            // cpu
            let total = (stat.utime + stat.stime) / *CLK_TCK as u64;
            let past = self.cpu_total.get();

            self.cpu_total.inc_by(total.saturating_sub(past));

            // threads
            self.threads.set(stat.num_threads);
        }
    }
}

impl Collector for ProcessCollector {
    fn encode(
        &self,
        mut encoder: prometheus_client::encoding::DescriptorEncoder,
    ) -> Result<(), std::fmt::Error> {
        let unit_fd = Unit::Other("fds".to_string());

        self.process_collector();

        let fd_encoder = encoder.encode_descriptor(
            "process_open",
            "Number of open file descriptors.",
            Some(&unit_fd),
            self.open_fds.metric_type(),
        )?;
        self.open_fds.encode(fd_encoder)?;

        let fd_encoder = encoder.encode_descriptor(
            "process_max",
            "Maximum number of open file descriptors.",
            Some(&unit_fd),
            self.max_fds.metric_type(),
        )?;
        self.max_fds.encode(fd_encoder)?;

        let cpu_total_encoder = encoder.encode_descriptor(
            "process_cpu_seconds_total",
            "Total user and system CPU time spent in seconds.",
            None,
            self.cpu_total.metric_type(),
        )?;
        self.cpu_total.encode(cpu_total_encoder)?;

        let vsize_encoder = encoder.encode_descriptor(
            "process_virtual_memory",
            "Virtual memory size in bytes.",
            Some(&Unit::Bytes),
            self.vsize.metric_type(),
        )?;
        self.vsize.encode(vsize_encoder)?;

        let rss_encoder = encoder.encode_descriptor(
            "process_resident_memory",
            "Resident memory size in bytes.",
            Some(&Unit::Bytes),
            self.rss.metric_type(),
        )?;
        self.rss.encode(rss_encoder)?;

        let start_time_encoder = encoder.encode_descriptor(
            "process_start_time",
            "Start time of the process since unix epoch in seconds.",
            Some(&Unit::Seconds),
            self.start_time.metric_type(),
        )?;
        self.start_time.encode(start_time_encoder)?;

        let threads_encoder = encoder.encode_descriptor(
            "process_threads",
            "Number of threads in the process.",
            None,
            self.threads.metric_type(),
        )?;
        self.threads.encode(threads_encoder)?;

        Ok(())
    }
}
