use std::collections::HashMap;

use diesel::{
    backend::Backend,
    deserialize::{FromSql, FromSqlRow},
    expression::AsExpression,
    pg::Pg,
    prelude::{Associations, Identifiable, Insertable},
    serialize::{Output, ToSql},
    sql_types::Jsonb,
    Queryable, Selectable,
};
use uuid::Uuid;

use crate::{resource::resource_manager::resource_manager, schema::*};

#[derive(Queryable, PartialEq, PartialOrd, Eq, Ord, Debug, Clone)]
pub struct User {
    pub id: uuid::Uuid,
    pub username: String,
    pub email: String,
    pub firstname: String,
    pub lastname: String,
    pub picture: Option<String>,
    pub locale: Option<String>,
    pub password: Option<String>,
}

pub type Actions = Vec<String>;

// Model for the "principal" table
#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(primary_key(user_id))]
#[diesel(table_name = principal)]
pub struct Principal {
    pub user_id: Uuid,
    pub allowed_domains: Vec<Option<String>>,
    pub all_domains_allowed: bool,
}

// Model for the "permission" table
#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(primary_key(permission_name))]
#[diesel(table_name = permission)]
pub struct Permission {
    pub permission_name: String,
    pub resource_name: String,
    pub actions: Vec<Option<String>>,
    pub effect: Effect,
}

// Model for the "user_permission" table
#[derive(Queryable, Selectable, Identifiable, Associations, Debug, PartialEq)]
#[diesel(primary_key(principal_id, permission_name, domain))]
#[diesel(belongs_to(Principal, foreign_key = principal_id))]
#[diesel(table_name = user_permission)]
pub struct UserPermission {
    pub principal_id: Uuid,
    pub permission_name: String,
    pub domain: String,
    pub resource_name: String,
    pub actions: Vec<Option<String>>,
    pub effect: Effect,
}

// Model for the "role" table
#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(primary_key(name, domain))]
#[diesel(table_name = role)]
pub struct Role {
    pub name: String,
    pub domain: String,
    pub description: String,
}

// Model for the "user_role" table
#[derive(Queryable, Identifiable, Associations, Debug, PartialEq)]
#[diesel(primary_key(principal_id, role_name, domain))]
#[diesel(belongs_to(Principal, foreign_key = principal_id))]
#[diesel(table_name = user_role)]
pub struct UserRole {
    pub principal_id: Uuid,
    pub role_name: String,
    pub domain: String,
}

// Model for the "role_permission" table
#[derive(Queryable, Identifiable, Debug, PartialEq)]
#[diesel(primary_key(role_name, permission_name, domain))]
#[diesel(belongs_to(Permission, foreign_key = permission_name))]
#[diesel(table_name = role_permission)]
pub struct RolePermission {
    pub role_name: String,
    pub permission_name: String,
    pub domain: String,
}

#[derive(Debug, PartialEq, diesel_derive_enum::DbEnum)]
#[ExistingTypePath = "crate::schema::sql_types::PermissionEffect"]

pub enum Effect {
    Allow,
    Deny,
}

impl From<String> for Effect {
    fn from(s: String) -> Self {
        match s.to_ascii_lowercase().as_str() {
            "allow" => Self::Allow,
            "deny" => Self::Deny,
            _ => Self::Deny,
        }
    }
}

impl From<&str> for Effect {
    fn from(s: &str) -> Self {
        match s.to_ascii_lowercase().as_str() {
            "allow" => Self::Allow,
            "deny" => Self::Deny,
            _ => Self::Deny,
        }
    }
}

#[derive(Insertable)]
#[diesel(table_name = resource_reference)]
pub struct NewResourceReference<'a> {
    pub resource_name: &'a str,
    pub domain: &'a str,
    pub reference_id: &'a Uuid,
    pub actions: &'a Actions,
    pub attributes: &'a ResourceReferenceAttributes,
}

#[derive(Insertable)]
#[diesel(table_name = service_instance)]
pub struct NewServiceInstance<'a> {
    pub name: &'a str,
    pub service_id: &'a Uuid,
    pub health_check_url: &'a str,
}

#[derive(Insertable)]
#[diesel(table_name = service_resource_claim)]
pub struct NewServiceResourceClaim<'a> {
    pub service_name: &'a str,
    pub service_id: &'a Uuid,
    pub resource_name: &'a str,
    pub reference_id: &'a Uuid,
}

#[derive(Queryable, Selectable, Identifiable, Debug, PartialEq)]
#[diesel(table_name = resource_reference)]
#[diesel(primary_key(resource_name, domain, reference_id))]
pub struct ResourceReference {
    pub resource_name: String,
    pub domain: String,
    pub reference_id: Uuid,
    pub actions: Vec<Option<String>>,
    pub attributes: ResourceReferenceAttributes,
}

#[derive(
    FromSqlRow, AsExpression, serde::Serialize, serde::Deserialize, Debug, Default, Clone, PartialEq,
)]
#[diesel(sql_type = diesel::sql_types::Jsonb)]
pub struct ResourceReferenceAttributes {
    pub constraints: HashMap<String, ResourceConstraint>,
}

impl<DB: Backend> FromSql<Jsonb, DB> for ResourceReferenceAttributes
where
    serde_json::Value: FromSql<Jsonb, DB>,
{
    fn from_sql(bytes: DB::RawValue<'_>) -> diesel::deserialize::Result<Self> {
        serde_json::from_value(serde_json::Value::from_sql(bytes)?).map_err(Into::into)
    }
}

impl ToSql<Jsonb, Pg> for ResourceReferenceAttributes
where
    serde_json::Value: ToSql<Jsonb, Pg>,
{
    fn to_sql<'b>(&'b self, out: &mut Output<'b, '_, Pg>) -> diesel::serialize::Result {
        let value = serde_json::to_value(self)?;
        let mut out = Output::reborrow(out);
        <serde_json::Value as ToSql<Jsonb, Pg>>::to_sql(&value, &mut out)
    }
}

#[derive(serde::Serialize, serde::Deserialize, Debug, Default, Clone, PartialEq)]
pub struct ResourceConstraint {}

impl From<resource_manager::Constraint> for ResourceConstraint {
    fn from(_constraint: resource_manager::Constraint) -> Self {
        Self {}
    }
}
