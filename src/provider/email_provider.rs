use std::{fmt::Debug, sync::Arc};

use async_trait::async_trait;
use serde::Deserialize;
use tracing::debug;

use crate::{
    user_service::{UserService, UserServiceError},
    ProviderAuthentication, ProviderError, ProviderSpecificError,
};

use super::Provider;

use argon2::{Argon2, Params, PasswordHash, PasswordVerifier};

#[derive(Deserialize)]
pub struct EmailProvider {
    email: String,
    password: String,
}

#[derive(Debug)]
pub enum EmailProviderError {
    FailedToFetchUser(Box<dyn std::error::Error>),
    NoUserFound,
    WrongPassword,
    InternalError(Box<dyn std::error::Error>),
}

impl std::fmt::Display for EmailProviderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            EmailProviderError::FailedToFetchUser(e) => write!(f, "failed to fetch user: {:?}", e),
            EmailProviderError::NoUserFound => write!(f, "no user found"),
            EmailProviderError::WrongPassword => write!(f, "wrong password"),
            EmailProviderError::InternalError(e) => write!(f, "internal error: {:?}", e),
        }
    }
}

impl ProviderSpecificError for EmailProviderError {
    fn name(&self) -> String {
        "email".to_string()
    }

    fn wrap_specific_error(self) -> ProviderError {
        ProviderError::ProviderSpecificError(Box::new(self))
    }
}

impl std::error::Error for EmailProviderError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            EmailProviderError::FailedToFetchUser(ref e) => Some(e.as_ref()),
            EmailProviderError::NoUserFound => None,
            EmailProviderError::WrongPassword => None,
            EmailProviderError::InternalError(ref e) => Some(e.as_ref()),
        }
    }
}

#[async_trait]
impl Provider for EmailProvider {
    async fn authorize(
        &self,
        user_service: Arc<dyn UserService + Sync + Send>,
    ) -> Result<ProviderAuthentication, ProviderError> {
        debug!(
            "Authorizing with email provider: {}, {}",
            self.email, self.password
        );

        let user = user_service
            .get_user(&self.email)
            .await
            .map_err(|e| match e {
                UserServiceError::UserNotFound => {
                    EmailProviderError::NoUserFound.wrap_specific_error()
                }
                UserServiceError::FailedToFetchUser(e) => {
                    EmailProviderError::FailedToFetchUser(e).wrap_specific_error()
                }
            })?;

        let hash = match &user.password {
            Some(p) => PasswordHash::new(p.as_str()),
            None => return Err(EmailProviderError::WrongPassword.wrap_specific_error()), // Actual no password set to be correct
        }
        .map_err(|e| EmailProviderError::InternalError(Box::from(e)).wrap_specific_error())?;

        let argon = Argon2::new(
            argon2::Algorithm::Argon2id,
            argon2::Version::V0x13,
            Params::default(),
        );

        argon
            .verify_password(self.password.as_bytes(), &hash)
            .map_err(|_| EmailProviderError::WrongPassword.wrap_specific_error())?;

        Ok(ProviderAuthentication { user: user.clone() })
    }

    fn parse_data(data: super::ProviderData) -> Result<Self, ProviderError> {
        match data.content_type.as_str() {
            "application/json" => serde_json::from_slice::<EmailProvider>(&data.data)
                .map_err(|e| ProviderError::from(e)),
            _ => Err(ProviderError::ContentTypeNotAllowedError),
        }
    }
}
