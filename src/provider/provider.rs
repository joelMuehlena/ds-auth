use std::{error, fmt, sync::Arc};

use actix_web::web;
use async_trait::async_trait;

use crate::{models::User, user_service::UserService};

#[derive(Debug)]
pub enum ProviderError {
    ParseError(Box<dyn std::error::Error>),
    ContentTypeNotAllowedError,
    ProviderSpecificError(Box<dyn ProviderSpecificError>),
    OtherError,
}

impl From<serde_json::Error> for ProviderError {
    fn from(value: serde_json::Error) -> Self {
        ProviderError::ParseError(Box::from(value))
    }
}

impl fmt::Display for ProviderError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            ProviderError::ParseError(e) => {
                write!(f, "Failed parsing provider data: {:?}", e.as_ref())
            }
            ProviderError::ContentTypeNotAllowedError => {
                write!(f, "The specified content type is not allowed")
            }
            ProviderError::OtherError => write!(f, "An unexpected error occurred"),
            ProviderError::ProviderSpecificError(e) => {
                write!(f, "An provider error occurred: {:?}", e.as_ref())
            }
        }
    }
}

impl error::Error for ProviderError {
    fn source(&self) -> Option<&(dyn error::Error + 'static)> {
        match self {
            ProviderError::ParseError(e) => Some(e.as_ref()),
            ProviderError::ContentTypeNotAllowedError => None,
            ProviderError::ProviderSpecificError(e) => e.source(),
            ProviderError::OtherError => None,
        }
    }
}

pub trait ProviderSpecificError: error::Error {
    fn name(&self) -> String;
    fn wrap_specific_error(self) -> ProviderError;
}

#[derive(Debug)]
pub struct ProviderData {
    pub content_type: String,
    pub data: web::BytesMut,
}

#[derive(Debug)]
pub struct ProviderAuthentication {
    pub user: User,
}

#[async_trait]
pub trait Provider {
    fn parse_data(data: ProviderData) -> Result<Self, ProviderError>
    where
        Self: Sized;
    async fn authorize(
        &self,
        user_service: Arc<dyn UserService + Sync + Send>,
    ) -> Result<ProviderAuthentication, ProviderError>;
}
