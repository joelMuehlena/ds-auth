use std::collections::HashMap;

use diesel::prelude::*;
use diesel::{ExpressionMethods, RunQueryDsl};
use tracing::debug;
use uuid::Uuid;

use crate::resource::resource_manager::resource_manager;

use crate::{models, schema, DBPool};

#[derive(Debug, Clone)]
pub struct ResourceReference {
    pub domain: String,
    pub reference_id: uuid::Uuid,
    pub actions: models::Actions,
    pub attributes: models::ResourceReferenceAttributes,
}

impl From<models::ResourceReference> for ResourceReference {
    fn from(r: models::ResourceReference) -> Self {
        Self {
            domain: r.domain,
            reference_id: r.reference_id,
            actions: r
                .actions
                .into_iter()
                .map(|a| a.unwrap_or_default())
                .collect(),
            attributes: r.attributes,
        }
    }
}

impl ResourceReference {
    pub fn from_proto_resource(
        resource: resource_manager::Resource,
    ) -> Result<Self, anyhow::Error> {
        let reference_id =
            Uuid::parse_str(&resource.reference_id.unwrap_or(Uuid::nil().to_string()))?;

        Ok(Self {
            domain: resource.domain,
            reference_id,
            actions: resource.actions,
            attributes: models::ResourceReferenceAttributes {
                constraints: resource
                    .attributes
                    .unwrap()
                    .constraints
                    .into_iter()
                    .map(|(k, v)| (k, v.into()))
                    .collect::<HashMap<String, models::ResourceConstraint>>(),
            },
        })
    }
}

#[derive(Debug, Clone)]
pub struct Resource {
    pub name: String,
    pub references: Vec<ResourceReference>,
}

impl Resource {
    pub fn get(pool: DBPool, name: &String) -> Result<Self, anyhow::Error> {
        let mut conn = pool.get()?;

        let references = schema::resource_reference::table
            .filter(schema::resource_reference::resource_name.eq(name))
            .select(schema::resource_reference::all_columns)
            .load::<models::ResourceReference>(&mut conn)?;

        Ok(Self {
            name: name.clone(),
            references: references
                .into_iter()
                .map(ResourceReference::from)
                .collect(),
        })
    }

    pub fn register(
        pool: DBPool,
        service_name: String,
        service_id: Uuid,
        health_check_url: String,
        resource_name: String,
        mut reference: ResourceReference,
    ) -> Result<Uuid, anyhow::Error> {
        reference.reference_id = Uuid::new_v4();

        let mut conn = pool.get()?;

        let new_resource_reference = models::NewResourceReference {
            resource_name: &resource_name,
            domain: &reference.domain,
            reference_id: &reference.reference_id,
            actions: &reference.actions,
            attributes: &reference.attributes,
        };

        let new_service_instance = models::NewServiceInstance {
            name: &service_name,
            service_id: &service_id,
            health_check_url: &health_check_url,
        };

        let new_resource_claim = models::NewServiceResourceClaim {
            service_name: &service_name,
            service_id: &service_id,
            reference_id: &reference.reference_id,
            resource_name: &resource_name,
        };

        diesel::insert_into(schema::resource_reference::table)
            .values(&new_resource_reference)
            .on_conflict_do_nothing()
            .execute(&mut conn)?;

        diesel::insert_into(schema::service_instance::table)
            .values(&new_service_instance)
            .on_conflict_do_nothing()
            .execute(&mut conn)?;

        diesel::insert_into(schema::service_resource_claim::table)
            .values(&new_resource_claim)
            .on_conflict_do_nothing()
            .execute(&mut conn)?;

        Ok(reference.reference_id)
    }

    pub fn unregister(
        pool: DBPool,
        service_name: String,
        service_id: Uuid,
        resource_name: String,
        reference_id: Uuid,
    ) -> Result<(), anyhow::Error> {
        let mut conn = pool.get()?;

        diesel::delete(schema::resource_reference::table)
            .filter(
                schema::resource_reference::resource_name
                    .eq(&resource_name)
                    .and(schema::resource_reference::reference_id.eq(&reference_id)),
            )
            .execute(&mut conn)?;

        let remaining_service_claims: i64 =
            schema::service_resource_claim::dsl::service_resource_claim
                .filter(
                    schema::service_resource_claim::reference_id
                        .eq(&reference_id)
                        .and(schema::service_resource_claim::service_name.eq(&service_name))
                        .and(schema::service_resource_claim::service_id.eq(&service_id)),
                )
                .count()
                .first(&mut conn)?;

        if remaining_service_claims == 0 {
            debug!("Remaining service claims are zero so removing service instance");

            diesel::delete(schema::service_instance::table)
                .filter(
                    schema::service_instance::service_id
                        .eq(&service_id)
                        .and(schema::service_instance::name.eq(&service_name)),
                )
                .execute(&mut conn)?;
        }

        Ok(())
    }

    pub fn actions_per_domain(&self, domains: Vec<&String>) -> HashMap<String, Vec<String>> {
        self.references
            .iter()
            .filter(|r| domains.contains(&&r.domain))
            .fold(HashMap::new(), |mut acc, r| {
                acc.insert(r.domain.clone(), r.actions.clone());
                acc
            })
    }

    #[allow(dead_code)]
    pub fn actions(&self, domain: Option<&String>) -> Vec<String> {
        self.references
            .iter()
            .filter(|r| match domain {
                Some(d) => r.domain == *d,
                None => true,
            })
            .flat_map(|r| r.actions.clone())
            .collect()
    }

    #[allow(dead_code)]
    pub fn domains(&self) -> Vec<String> {
        self.references
            .iter()
            .map(|r| r.domain.clone())
            .collect::<Vec<String>>()
    }

    pub fn attributes(&self, domains: Vec<&String>) -> models::ResourceReferenceAttributes {
        let mut attributes = models::ResourceReferenceAttributes::default();

        self.references
            .iter()
            .filter(|r| domains.contains(&&r.domain))
            .for_each(|r| {
                r.attributes.constraints.iter().for_each(|(k, v)| {
                    attributes.constraints.insert(k.clone(), v.clone());
                });
            });

        attributes
    }
}
