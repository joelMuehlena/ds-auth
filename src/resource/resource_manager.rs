use resource_manager::resource_manager_server::ResourceManager as ResourceManagerTrait;
use resource_manager::{RegisterRequest, RegisterResponse, UnregisterRequest, UnregisterResponse};
use uuid::Uuid;

use crate::resource::{Resource, ResourceReference};
use crate::DBPool;

pub mod resource_manager {
    tonic::include_proto!("auth_server.resource");
}

#[derive(Debug)]
pub struct ResourceManager {
    pool: DBPool,
}

impl ResourceManager {
    pub fn new(pool: DBPool) -> Self {
        Self { pool }
    }
}

#[tonic::async_trait]
impl ResourceManagerTrait for ResourceManager {
    async fn register(
        &self,
        request: tonic::Request<RegisterRequest>,
    ) -> std::result::Result<tonic::Response<RegisterResponse>, tonic::Status> {
        let (_req_meta, _req_ext, req) = request.into_parts();

        if req.resource.is_none() {
            return Err(tonic::Status::invalid_argument(
                "Missing resource in request",
            ));
        }

        let svc_id = Uuid::parse_str(&req.service_id).map_err(|e| {
            tonic::Status::invalid_argument(format!("Failed to parse service_id: {:?}", e))
        })?;

        let resource = req.resource.unwrap();
        let resource_name = resource.name.clone();

        let resource_reference = ResourceReference::from_proto_resource(resource);

        match resource_reference {
            Ok(resource_reference) => {
                let reference_id = Resource::register(
                    self.pool.clone(),
                    req.service_name,
                    svc_id,
                    req.health_check_url,
                    resource_name,
                    resource_reference,
                );

                match reference_id {
                    Ok(reference_id) => Ok(tonic::Response::new(RegisterResponse {
                        reference_id: reference_id.to_string(),
                        resource: None,
                    })),
                    Err(e) => {
                        return Err(tonic::Status::internal(format!(
                            "Failed to register resource: {:?}",
                            e
                        )))
                    }
                }
            }
            Err(e) => Err(tonic::Status::internal(format!(
                "Failed to parse resource: {:?}",
                e
            ))),
        }
    }

    async fn unregister(
        &self,
        request: tonic::Request<UnregisterRequest>,
    ) -> std::result::Result<tonic::Response<UnregisterResponse>, tonic::Status> {
        let (_req_meta, _req_ext, req) = request.into_parts();

        if req.reference_id.is_empty() {
            return Err(tonic::Status::invalid_argument(
                "Missing reference_id in request",
            ));
        }

        let reference_id = Uuid::parse_str(&req.reference_id).map_err(|e| {
            tonic::Status::invalid_argument(format!("Failed to parse reference_id: {:?}", e))
        })?;

        let svc_id = Uuid::parse_str(&req.service_id).map_err(|e| {
            tonic::Status::invalid_argument(format!("Failed to parse service_id: {:?}", e))
        })?;

        Resource::unregister(
            self.pool.clone(),
            req.service_name,
            svc_id,
            req.resource_name.clone(),
            reference_id,
        )
        .map_err(|e| tonic::Status::internal(format!("Failed to unregister resource: {:?}", e)))?;

        Ok(tonic::Response::new(UnregisterResponse {
            success: true,
            reference_id: req.reference_id,
            name: req.resource_name,
        }))
    }
}
