// @generated automatically by Diesel CLI.

pub mod sql_types {
    #[derive(diesel::sql_types::SqlType)]
    #[diesel(postgres_type(name = "permission_effect"))]
    pub struct PermissionEffect;
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::PermissionEffect;

    permission (permission_name) {
        permission_name -> Text,
        resource_name -> Text,
        actions -> Array<Nullable<Text>>,
        effect -> PermissionEffect,
    }
}

diesel::table! {
    principal (user_id) {
        user_id -> Uuid,
        allowed_domains -> Array<Nullable<Text>>,
        all_domains_allowed -> Bool,
    }
}

diesel::table! {
    resource_reference (resource_name, domain, reference_id) {
        resource_name -> Text,
        domain -> Text,
        reference_id -> Uuid,
        actions -> Array<Nullable<Text>>,
        attributes -> Jsonb,
    }
}

diesel::table! {
    role (name, domain) {
        name -> Text,
        domain -> Text,
        description -> Text,
    }
}

diesel::table! {
    role_permission (role_name, permission_name, domain) {
        role_name -> Text,
        permission_name -> Text,
        domain -> Text,
    }
}

diesel::table! {
    service_instance (name, service_id) {
        name -> Text,
        service_id -> Uuid,
        health_check_url -> Text,
        created_at -> Timestamptz,
        updated_at -> Timestamptz,
    }
}

diesel::table! {
    service_resource_claim (service_name, service_id, reference_id, resource_name) {
        service_name -> Text,
        service_id -> Uuid,
        reference_id -> Uuid,
        resource_name -> Text,
    }
}

diesel::table! {
    user (id) {
        id -> Uuid,
        #[max_length = 64]
        username -> Varchar,
        #[max_length = 64]
        email -> Varchar,
        #[max_length = 128]
        firstname -> Varchar,
        #[max_length = 255]
        lastname -> Varchar,
        picture -> Nullable<Text>,
        #[max_length = 8]
        locale -> Nullable<Varchar>,
        password -> Nullable<Text>,
    }
}

diesel::table! {
    use diesel::sql_types::*;
    use super::sql_types::PermissionEffect;

    user_permission (principal_id, permission_name, domain) {
        principal_id -> Uuid,
        permission_name -> Text,
        domain -> Text,
        resource_name -> Text,
        actions -> Array<Nullable<Text>>,
        effect -> PermissionEffect,
    }
}

diesel::table! {
    user_role (principal_id, role_name, domain) {
        principal_id -> Uuid,
        role_name -> Text,
        domain -> Text,
    }
}

diesel::joinable!(role_permission -> permission (permission_name));
diesel::joinable!(user_permission -> principal (principal_id));
diesel::joinable!(user_role -> principal (principal_id));

diesel::allow_tables_to_appear_in_same_query!(
    permission,
    principal,
    resource_reference,
    role,
    role_permission,
    service_instance,
    service_resource_claim,
    user,
    user_permission,
    user_role,
);
