use async_trait::async_trait;
use serde::{Deserialize, Serialize};

use crate::{
    meta::{self, MetaInfo},
    models::User,
};

#[derive(Debug)]
pub enum UserServiceError {
    UserNotFound,
    FailedToFetchUser(Box<dyn std::error::Error>),
}

impl std::fmt::Display for UserServiceError {
    fn fmt(&self, f: &mut std::fmt::Formatter<'_>) -> std::fmt::Result {
        match self {
            UserServiceError::UserNotFound => write!(f, "User not found"),
            UserServiceError::FailedToFetchUser(e) => {
                write!(f, "Failed to fetch user: {:?}", e.as_ref())
            }
        }
    }
}

impl std::error::Error for UserServiceError {
    fn source(&self) -> Option<&(dyn std::error::Error + 'static)> {
        match self {
            UserServiceError::UserNotFound => None,
            UserServiceError::FailedToFetchUser(e) => Some(e.as_ref()),
        }
    }
}

#[derive(Debug, Serialize, Clone, Deserialize)]
pub struct Meta {}

#[typetag::serde(name = "user_service_meta")]
impl meta::Meta for Meta {}

#[async_trait]
pub trait UserService: MetaInfo {
    async fn get_user(&self, email: &String) -> Result<User, UserServiceError>;
    async fn get_user_by_id(&self, uid: uuid::Uuid) -> Result<User, UserServiceError>;
}
